package calculator.nylinary.com

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import calculator.nylinary.com.databinding.ActivityMainBinding
import net.objecthunter.exp4j.ExpressionBuilder
import kotlin.math.ceil
import kotlin.math.floor

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    private var is_error : Boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.btn0.setOnClickListener {
            fixState()
            setChar("0")
        }
        binding.btn1.setOnClickListener {
            fixState()
            setChar("1")
        }
        binding.btn2.setOnClickListener {
            fixState()
            setChar("2")
        }
        binding.btn3.setOnClickListener {
            fixState()
            setChar("3")
        }
        binding.btn4.setOnClickListener {
            fixState()
            setChar("4")
        }
        binding.btn5.setOnClickListener {
            fixState()
            setChar("5")
        }
        binding.btn6.setOnClickListener {
            fixState()
            setChar("6")
        }
        binding.btn7.setOnClickListener {
            fixState()
            setChar("7")
        }
        binding.btn8.setOnClickListener {
            fixState()
            setChar("8")
        }
        binding.btn9.setOnClickListener {
            fixState()
            setChar("9")
        }
        binding.subtractBtn.setOnClickListener {
            setChar("-")
        }
        binding.addBtn.setOnClickListener {
            setChar("+")
        }
        binding.divideBtn.setOnClickListener {
            setChar("/")
        }
        binding.multiplyBtn.setOnClickListener {
            setChar("*")
        }
        binding.opBtn.setOnClickListener {
            setChar("(")
        }
        binding.cpBtn.setOnClickListener {
            setChar(")")
        }
        binding.decimalBtn.setOnClickListener {
            setChar(".")
        }
        binding.acBtn.setOnClickListener {
            fixState()
            binding.mathOperation.text = ""
            binding.resultText.text = "0"
        }
        binding.deleteBtn.setOnClickListener {
            fixState()
            val tempText: String = binding.mathOperation.text.toString()
            if (tempText.isNotEmpty()) {
                binding.mathOperation.text = tempText.dropLast(1)
            }
        }
        binding.equalBtn.setOnClickListener {
            var result_text: String = ""
            result_text = try {
                val expression = ExpressionBuilder(binding.mathOperation.text.toString()).build()
                val result = expression.evaluate()
                if (ceil(result) == floor(result)) {
                    result.toInt().toString()
                } else {
                    result.toString()
                }
            } catch (e: Exception) {
                is_error = true
                "Error"
            }
            binding.resultText.text = result_text
        }
    }

    private fun fixState() {
        if (is_error) {
            is_error = false
            binding.resultText.text = "0"
        }
    }

    private fun setChar(char: String) {
        binding.mathOperation.append(char)
    }
}